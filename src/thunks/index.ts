import {authThunks} from "./authThunks";
import {flightsThunks} from "./flightsThunks";

export const thunks = {
  authThunks,
  flightsThunks
}

