import {flightsActions} from "../actions/flightsActions";

import OpenSkyApi from "../api/OpenSkyApi";
import {ThunkAction} from "redux-thunk";
import {Flight, State} from "../interfaces/interfaces";
import {Action} from "redux";


interface resProps {
  data: Flight[]
}

const getFlights = (start:Date):ThunkAction<void, State, unknown, Action> => {
  return dispatch => {
    dispatch(flightsActions.flightsRequest());
    OpenSkyApi.getFlights(start)
      .then((res: resProps) => dispatch(flightsActions.flightsSuccess(res.data)))
      .catch((err: object)=>flightsActions.flightsError(err))
  }
};

export const flightsThunks = {
  getFlights
};