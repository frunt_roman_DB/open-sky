import {authActions} from "../actions/authActions";
import {mockLoginData} from "../api/loginData";
import {setUser} from "../utils/localStorage";
import {ThunkAction} from "redux-thunk";
import {State} from "../interfaces/interfaces";
import {Action} from "redux";


const login = (user:string, password: string):ThunkAction<void, State, unknown, Action> => {
  return dispatch => {
    dispatch(authActions.loginRequest(user, password));
    if (user === mockLoginData.user && password === mockLoginData.password) {
      dispatch(authActions.loginSuccess());
      setUser({user: 'admin', id: 1});
    } else dispatch(authActions.loginError());

  }
};

export const authThunks = {
  login
};