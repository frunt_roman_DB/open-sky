export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_ERROR = 'LOGIN_ERROR';

// action creators

const loginRequest = (user: string, password: string) => ({
  type: LOGIN_REQUEST,
  payload: {
    user,
    password
  }
});

const loginSuccess = () => ({
  type: LOGIN_SUCCESS,
});

const loginError = () => ({
  type: LOGIN_ERROR,
});

export const authActions = {
  loginRequest,
  loginSuccess,
  loginError
};

