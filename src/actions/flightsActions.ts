import {Flight} from "../interfaces/interfaces";

export const FLIGHTS_REQUEST = 'FLIGHTS_REQUEST';
export const FLIGHTS_SUCCESS = 'FLIGHTS_SUCCESS';
export const FLIGHTS_ERROR = 'FLIGHTS_ERROR';

// action creators

const flightsRequest = () => ({
  type: FLIGHTS_REQUEST,
});

const flightsSuccess = (flights: Flight[]) => ({
  type: FLIGHTS_SUCCESS,
  flights
});

const flightsError = (error: object) => ({
  type: FLIGHTS_ERROR,
  error
});

export const flightsActions = {
  flightsRequest,
  flightsSuccess,
  flightsError
};

