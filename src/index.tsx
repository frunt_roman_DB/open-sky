import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import reportWebVitals from './reportWebVitals';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from "redux-thunk";
import {reducer} from "./reducers";
import {Provider} from "react-redux";

// components
import {Login} from "./components/Login/Login";
import {Dashboard} from "./components/Dashboard/Dashboard";
import {PrivateRoute} from "./routes/PrivateRoute/PrivateRoute";

const store = createStore(reducer, composeWithDevTools(
  applyMiddleware(thunk),
));


ReactDOM.render(
  <Provider store={store}>
    <React.StrictMode>
      <Router>
        <Switch>
          <Route exact path="/">
            <Redirect to="/login" />
          </Route>
          <Route component={Login} exact path={'/login'} />
          <PrivateRoute component={Dashboard} path={'/dashboard'} />
        </Switch>
      </Router>

    </React.StrictMode>
  </Provider>
  ,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
