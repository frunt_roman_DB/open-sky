import { FLIGHTS_SUCCESS, FLIGHTS_ERROR, FLIGHTS_REQUEST } from "../actions/flightsActions";
import {Flight, State} from "../interfaces/interfaces";

const initialState = {
  flights: [],
  loadingInProgress: false,
  flightsError: false,
};

interface Action {
  type: string,
  flights: Flight[],
}

export const flightsReducer = (state = initialState, action: Action) => {
  switch (action.type) {
    case (FLIGHTS_SUCCESS as string):
      localStorage.setItem('user', 'admin');
      return {
        ...state,
        loadingInProgress: false,
        flightsError: false,
        flights: action.flights
      }
    case (FLIGHTS_ERROR): {
      return {
        ...state,
        flightsError: true,
        loadingInProgress: false
      }
    }
    case (FLIGHTS_REQUEST): {
      return {
        ...state,
        flightsError: false,
        loadingInProgress: true,
      }
    }
    default:
      return {
        ...state,
      }
  }
};

export const flightsState = {
  flights: (state: State) => state.flightsReducer.flights,
  loadingInProgress: (state: State) => state.flightsReducer.loadingInProgress,
  flightsError: (state: State) => state.flightsReducer.flightsError,
};