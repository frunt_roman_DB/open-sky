import {LOGIN_ERROR, LOGIN_REQUEST, LOGIN_SUCCESS} from "../actions/authActions";
import {State} from "../interfaces/interfaces";

const initialState = {
  isLogged: localStorage.getItem('user'),
  loginError: false,
};

interface Action {
  type: string
}

export const authReducer = (state = initialState, action: Action) => {
  switch (action.type) {
    case (LOGIN_SUCCESS as string):
      return {
        ...state,
        isLogged: true,
        loginError: false,
      }
    case (LOGIN_ERROR): {
      return {
        ...state,
        loginError: true,
      }
    }
    case (LOGIN_REQUEST): {
      return {
        ...state,
        loginError: false
      }
    }
    default:
      return {
        ...state,
      }
  }
};

export const authState = {
  isLogged: (state: State) => state.authReducer.isLogged,
  loginError: (state: State) => state.authReducer.loginError,
};