import {combineReducers} from "redux";
import {authReducer} from "./authReducer";
import {flightsReducer} from "./flightsReducer";

export const reducer = combineReducers({
  authReducer,
  flightsReducer
})