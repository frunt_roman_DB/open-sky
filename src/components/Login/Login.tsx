import React, {useState, FormEvent} from "react";
import {Alert, Button, Col, Form, FormControl, Row} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {thunks} from "../../thunks";
import {authState} from "../../reducers/authReducer";
import {Redirect} from "react-router-dom";

export const Login:React.FC = () => {
  const [user, setUser] = useState('');
  const [password, setPassword] = useState('');
  const loginError = useSelector(authState.loginError);
  const isLogged = useSelector(authState.isLogged);
  const dispatch = useDispatch();
  const onSubmitHandler = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    dispatch(thunks.authThunks.login(user, password));
  };
  if (isLogged) {
    return <Redirect to={'/dashboard'} />
  }
  return (
    <Row>
      <Col sm={4}/>
      <Col sm={4}>
        <Form onSubmit={(e)=>onSubmitHandler(e)}>
          <FormControl
            value={user}
            onChange={(event) => setUser(event.target.value)}
            className={'mb-3 mt-5'}
            type={'text'}
          />
          <FormControl
            value={password}
            onChange={(event) => setPassword(event.target.value)}
            className={'mb-3'}
            type={'password'}
          />
          <Button className={'mb-3'} type={"submit"}>Submit</Button>
          {loginError ? <Alert variant={'danger'}>Invalid Data</Alert> : null}
        </Form>
      </Col>
      <Col sm={4}/>
    </Row>
  );
};