import React from "react";
import {Table} from "react-bootstrap";
import {Flight} from "../../interfaces/interfaces";


interface Props {
    page: number,
    flights: Array<Flight>,
    perPage: number
}


export const formatTimeISO = (value: number) => new Date(value * 1000).toISOString();

export const FlightsList: React.FC<Props> = ({page, flights, perPage}) => {
    const startIndex = page * perPage;
    const lastIndex = page * perPage + perPage;
    const flightsForRender = flights.filter((flight, index, flightsArray) => {
        return (index >= startIndex) && (index <= lastIndex);
    });

    return (
        <Table striped bordered hover>
            <thead>
            <tr>
                <th>Airport</th>
                <th>Arriving</th>
                <th>Departing</th>
            </tr>
            </thead>
            <tbody>
            {flightsForRender.map((item: Flight) => {
                return (
                    <tr key={item.icao24}>
                        <td>{item.estDepartureAirport || item.estArrivalAirport}</td>
                        <td>{formatTimeISO(item.firstSeen)}</td>
                        <td>{formatTimeISO(item.lastSeen)}</td>
                    </tr>
                );
            })}
            </tbody>
        </Table>
    );
};