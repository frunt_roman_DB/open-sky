import React, {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {flightsThunks} from "../../thunks/flightsThunks";
import 'react-calendar/dist/Calendar.css';
import {Button, Col, Container, Row} from "react-bootstrap";
import ReactPaginate from "react-paginate";
import {flightsState} from "../../reducers/flightsReducer";
import {FlightsList} from "../FlightsList/FlightsList";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";


export const Dashboard: React.FC = () => {
  const perPage = 10;
  const flights = useSelector(flightsState.flights);
  const isPending = useSelector(flightsState.loadingInProgress);
  const [page, setPage] = useState(0)
  const dispatch = useDispatch();
  const [day, setDay] = useState(new Date());
  const requestSend = () => {
    dispatch(flightsThunks.getFlights(day))
  };
  const onPageChange = (page: number) => {
    setPage(page);
  }
  const flightsRender = () => flights.length ?
    <>
      <FlightsList page={page} flights={flights} perPage={perPage}/>
      <ReactPaginate
        previousLabel={'<'}
        nextLabel={'>'}
        breakLabel={'...'}
        breakClassName={'break-me'}
        containerClassName={'pagination mx-auto'}
        activeClassName={'active'}
        previousLinkClassName={'btn btn-secondary'}
        nextLinkClassName={'btn btn-secondary'}
        pageClassName={'mr-3 ml-3 btn btn-secondary'}
        onPageChange={(e) => onPageChange(e.selected)}
        pageCount={Math.ceil(flights.length / perPage)}
        pageRangeDisplayed={3} marginPagesDisplayed={1}
      />
    </> : null
  return (
    <Container>
      <Row className={'mt-5 mb-3'}>
        <Col md={6}>
          <DatePicker
            selected={day}
            onChange={(date: Date) => setDay(date)} //only when value has changed
            showTimeSelect
          />
        </Col>
        <Col md={6}>
          <Button onClick={requestSend} variant={'primary'}>Request</Button>
        </Col>
      </Row>
      <Row>
        {isPending ?
          <div className="spinner-border" role="status">
            <span className="sr-only">Loading...</span>
          </div>
          : flightsRender()
        }
      </Row>

    </Container>
  );
};