import axios, {AxiosResponse}  from 'axios';

export default class Api {
  request = (
    body: object,
    url: string,
    method: string = 'get',
  ):Promise<AxiosResponse<any>> => {
    switch (method) {
      default:
        return axios.get(url, {
          params: body
        });
    }
  }
}