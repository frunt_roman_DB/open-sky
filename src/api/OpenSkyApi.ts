import Api from "./Api";

class OpenSkyApi extends Api{
  openSkyApiUrl = 'https://opensky-network.org/api';
  getFlights = (start: Date) => {
    const utcMillisecondsSinceEpoch = start.getTime() + (start.getTimezoneOffset() * 60 * 1000)
    const utcSecondsSinceEpoch = Math.round(utcMillisecondsSinceEpoch / 1000);
    return this.request(
      {
        begin: utcSecondsSinceEpoch,
        end: utcSecondsSinceEpoch + 7200
      },
      this.openSkyApiUrl + '/flights/all', 'get'
    );
  }
}

export default new OpenSkyApi();