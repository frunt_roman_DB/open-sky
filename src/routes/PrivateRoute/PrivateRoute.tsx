import React from "react";
import {Redirect, Route} from "react-router-dom";
import {useSelector} from "react-redux";
import {authState} from "../../reducers/authReducer";

interface PrivateRouteProps {
  component: any,
  path: string
}

export const PrivateRoute:React.FC<PrivateRouteProps> = ({component: Component, ...rest}) => {

  // Add your own authentication on the below line.
  const isLoggedIn = useSelector(authState.isLogged)
  return (
    <Route
      {...rest}
      render={props =>
        isLoggedIn ? (
          <Component {...props} />
        ) : (
          <Redirect to={{pathname: '/login'}}/>
        )
      }
    />
  )
}