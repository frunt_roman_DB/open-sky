const userKey = 'user';

export const setUser = (user: {user: string, id: number}) => {
    localStorage.setItem(userKey, JSON.stringify(user));
}

export const getUser = () => {
    JSON.parse(localStorage.getItem(userKey) as string);
}